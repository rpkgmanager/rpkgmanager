require 'rubygems'
require "open4"
require 'pp'
require 'tmpdir'

require APP_ROOT + '/task'
require APP_ROOT + '/package'


class AbstractPkgtools
	attr_reader :wantList, :tasks, :failedTasks, :cache;
	attr_accessor :depRebuild, :strictRebuild;
	
	def initialize()
		@cache = Hash.new();
		@wantList = Hash.new();
		
		@tasks = Array.new();
		@failedTasks = Array.new();
		@tasksPerName = Hash.new();
	end
	
	
	def cleanCache()
		@cache = Hash.new();
	end
	
	def setWantList(wantList)
		@wantList = wantList;
	end
	
	def gatherInstalledPkgs()
		# get info about the installed pkgs
		getInstalledPkgList().each { |pkgVer| getInstalledPkgData(pkgVer); }
	end
	
	def gatherData(wantList)
		# TODO - Marshal.dump and Marshal.load (save a copy of @cache to a file and only check packages that have a newer modified time in pkgsrc!)
		tmpStat = $curStatus[Thread.current];
		
		setWantList(wantList);
		@wantList.each { |pkgName, prio|
			pkg = getPkgData(pkgName);
			pkg.priority = prio;
		}
		
		# set up requiredBy, requiredByInstalled
		@cache.each { |pkgName, pkg|
			$curStatus[Thread.current] = "Setting requiredBy for dependencies of " + pkgName;
			
			pkg.depends.each { |depPkgName, depPkgHash| @cache[depPkgName].requiredBy[pkgName] = pkg; }
			pkg.buildDepends.each { |depPkgName, depPkgHash| @cache[depPkgName].requiredBy[pkgName] = pkg; }
			pkg.dependsInstalled.each { |depPkgName, depPkgHash| @cache[depPkgName].requiredByInstalled[pkgName] = pkg; }
		}
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def postGatherData()
		tmpStat = $curStatus[Thread.current];
		
		$curStatus[Thread.current] = "Checking for conflicts...";
		conflicts = conflictCheck();
		conflicts.each { |pkgName, patternMap|
			print pkgName + " (wanted by ";
			
			tmpStr = "";
			PP.singleline_pp(findTopForPkg(@cache[pkgName]), tmpStr);
			print tmpStr;
			
			puts ") conflicts with the following packages:";
			
			patternMap.each { |pattern, pkgNameArr|
				pkgNameArr.each { |tmpPkgName|
					print "\t" + tmpPkgName + " (wanted by ";
					
					tmpStr = "";
					PP.singleline_pp(findTopForPkg(@cache[tmpPkgName]), tmpStr);
					print tmpStr;
					
					puts "), matches pattern '" + pattern + "'";
				}
			}
		}
		if (!conflicts.empty?)
			raise "Conflicts detected!";
		end
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def findRootNodes(ignore = [])
		rootNodes = Hash.new();
		@cache.each { |pkgName, pkg|
			if (pkg.requiredByInstalled.empty? && pkg.requiredBy.empty?)
				rootNodes[pkgName] = pkg;
				next;
			end
			
			count = pkg.requiredByInstalled.length + pkg.requiredBy.length;
			ignore.each { |ignored|
				if (pkg.requiredByInstalled.has_key?(ignored))
					count -= 1;
				end
				
				if (pkg.requiredBy.has_key?(ignored))
					count -= 1;
				end
			}
			if (count == 0)
				rootNodes[pkgName] = pkg;
			end
		}
		rootNodes = rootNodes.sort { |a, b| -1 * (a[1].priority <=> b[1].priority) };
	end
	
	def deleteRootNodes()
		prev = nil;
		cur = findRootNodes();
		ignore = Array.new();
		# first anything that can be already removed without updating any package, should be removed immediately
		while (prev != cur)
			cur.each { |pkgName, pkg|
				if (@wantList.has_key?(pkgName) || @tasksPerName.has_key?(pkgName))
					next;
				end
				
				log("Create delete task for " + pkgName, "deleteRootNodes", 800);
				
				task = Task.new(self, @cache[pkgName], "delete");
				@tasks.push(task);
				@tasksPerName[pkgName] = Array.new()
				@tasksPerName[pkgName].push(task);
				ignore.push(pkgName);
			}
			
			prev = cur;
			cur = findRootNodes(ignore);
		end
	end
	
	def createTasks()
		tmpStat = $curStatus[Thread.current];
		
		$curStatus[Thread.current] = "Tasks to delete root nodes...";
		deleteRootNodes();
		
		# now we have to walk the tree and create the additional tasks
		$curStatus[Thread.current] = "Sorting cache...";
		sortedCache = @cache.sort { |a, b| -1 * (a[1].priority <=> b[1].priority) };
		
		$curStatus[Thread.current] = "Creating tasks from cache...";
		sortedCache.each { |pkgName, pkg|
			if (@cache.has_key?(pkgName))
				createTasksForPkg(pkg);
			end
		}
		
		$curStatus[Thread.current] = "Working on a summary of tasks...";
		taskSummary()
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def preExecTasks()
		tmpStat = $curStatus[Thread.current];
		
		cur = 0;
		cnt = @tasks.count;
		@tasks.delete_if { |task|
			$curStatus[Thread.current] = "preExec() for " + task.pkg.pkgName + "...";
			
			cur += 1;
			task.preExec(curTimeStr() + ": [" + cur.to_s() + "/" + cnt.to_s() + "]") == false
		}
		$curStatus[Thread.current] = tmpStat;
	end
	
	def execTasks()
		tmpStat = $curStatus[Thread.current];
		
		cur = 0;
		cnt = @tasks.count;
		@tasks.each { |task|
			$curStatus[Thread.current] = "Working on task for " + task.pkg.pkgName + "...";
			
			cur += 1;
			task.exec(curTimeStr() + ": [" + cur.to_s() + "/" + cnt.to_s() + "]");
		}
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def checkFailedTasks()
		tmpStat = $curStatus[Thread.current];
		
		$curStatus[Thread.current] = "Checking for failed tasks...";
		
		@failedTasks.each { |task|
			print "Task "
			print task.pkg.pkgName
			print " failed";
			if (task.errorFile != nil)
				print " (log at "
				print task.errorFile;
				print ")";
			end
			if (task.error.message != nil && task.error.message != "")
				puts " with message:"
				print task.error.class
				print ": " + task.error.message;
			end
			print "\n\n";
		}
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def taskSummary()
		tmpStat = $curStatus[Thread.current];
		$curStatus[Thread.current] = "Working on a summary of created tasks...";
		print "\n";
		
		cur = 1;
		cnt = @tasks.count;
		@tasks.each { |task|
			log("[" + cur.to_s() + "/" + cnt.to_s() + "] Task " + task.action + " for " + task.pkg.pkgName + "...", "taskSummary", 500);
			cur += 1;
		}
		
		
		tmpTasks = Array.new();
		@tasks.each { |task|
			if (task.action == "force-delete")
				tmpTasks.push(task);
			end
		}
		if (!tmpTasks.empty?)
			print "The following TEMPORARILY CONFLICTING packages will be forcefully removed: "
			first = true;
			tmpTasks.each { |task|
				if (first == true)
					first = false;
				else
					print ", ";
				end
				print task.pkg.pkgName;
			}
			print "\n\n";
		end
		
		tmpTasks = Array.new();
		@tasks.each { |task|
			if (task.action == "delete")
				tmpTasks.push(task);
			end
		}
		if (!tmpTasks.empty?)
			print "The following UNNEEDED packages will be removed: "
			first = true;
			tmpTasks.each { |task|
				if (first == true)
					first = false;
				else
					print ", ";
				end
				print task.pkg.pkgName;
			}
			print "\n\n";
		end
		
		
		tmpTasks = Array.new();
		@tasks.each { |task|
			if (task.action == "rebuild")
				tmpTasks.push(task);
			end
		}
		if (!tmpTasks.empty?)
			print "The following DIRTY packages will be rebuilt: "
			first = true;
			tmpTasks.each { |task|
				if (first == true)
					first = false;
				else
					print ", ";
				end
				print task.pkg.pkgName;
			}
			print "\n\n";
		end
		
		
		tmpTasks = Array.new();
		@tasks.each { |task|
			if (task.action == "upgrade")
				tmpTasks.push(task);
			end
		}
		if (!tmpTasks.empty?)
			print "The following UPDATED packages will be rebuilt: "
			first = true;
			tmpTasks.each { |task|
				if (first == true)
					first = false;
				else
					print ", ";
				end
				print task.pkg.pkgName;
			}
			print "\n\n";
		end
		
		
		tmpTasks = Array.new();
		@tasks.each { |task|
			if (task.action == "install")
				tmpTasks.push(task);
			end
		}
		if (!tmpTasks.empty?)
			print "The following MISSING packages will be installed: "
			first = true;
			tmpTasks.each { |task|
				if (first == true)
					first = false;
				else
					print ", ";
				end
				print task.pkg.pkgName;
			}
			print "\n\n";
		end
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	
	private
	
	
	def findTopForPkg(pkg)
		if (@wantList.has_key?(pkg.pkgName))
			return [pkg.pkgName];
		end
		
		topPkgs = Hash.new()
		pkg.requiredBy.each { |pkgName, tmpPkg|
			tmpPkgs = findTopForPkg(tmpPkg);
			if (tmpPkgs == nil)
				next;
			end
			
			tmpPkgs.each { |tmpPkgName, tmp|
				topPkgs[tmpPkgName] = tmpPkgName;
			}
		}
		
		return topPkgs.keys();
	end
	
	def getPackage(pkgName)
		if (@cache.has_key?(pkgName))
			return @cache[pkgName];
		end
		
		ret = Package.new(self, pkgName);
		
		@cache[pkgName] = ret;
		return @cache[pkgName];
	end
	
	def createTasksForPkg(startPkg, space = 0)
		prefix = "";
		space.times { |i| prefix += " "; }
		log(prefix + "Creating task for " + startPkg.pkgName, "createTasksForPkg", 990);
		
		if (@tasksPerName.has_key?(startPkg.pkgName) && @tasksPerName[startPkg.pkgName].last.action != "force-delete")
			log(prefix + "Found cached task for " + startPkg.pkgName, "createTasksForPkg", 990);
			return [@tasksPerName[startPkg.pkgName].last];
		end
		
		tmpStat = $curStatus[Thread.current];
		$curStatus[Thread.current] = "Creating task for " + startPkg.pkgName + "...";
		
		dependencies = Hash.new();
		dirtyDep = false;
		startPkg.depends.each { |depPkgName, depPkgHash|
			if (startPkg.installed? && @strictRebuild)
				if (!depPkgHash["pkg"].installed?)
					dirtyDep = true;
				elsif (depPkgHash["pkg"].buildDate != nil && startPkg.buildDate != nil && depPkgHash["pkg"].buildDate > startPkg.buildDate)
					dirtyDep = true;
				elsif (@tasksPerName.has_key?(depPkgHash["pkg"].pkgName))
					dirtyDep = true;
				end
			end
			
			createTasksForPkg(depPkgHash["pkg"], space + 2).each { |depTask|
				dependencies[depTask.pkg.pkgName] = depTask
				
				if (@depRebuild && startPkg.installed? && depTask.action != "rebuild")
					dirtyDep = true;
				end
			}
		}
		startPkg.buildDepends.each { |depPkgName, depPkgHash|
			if (startPkg.installed? && @strictRebuild)
				if (!depPkgHash["pkg"].installed?)
					dirtyDep = true;
				elsif (depPkgHash["pkg"].buildDate != nil && startPkg.buildDate != nil && depPkgHash["pkg"].buildDate > startPkg.buildDate)
					dirtyDep = true;
				elsif (@tasksPerName.has_key?(depPkgHash["pkg"].pkgName))
					dirtyDep = true;
				end
			end
			
			createTasksForPkg(depPkgHash["pkg"], space + 2).each { |depTask|
				dependencies[depTask.pkg.pkgName] = depTask
				
				if (@depRebuild && startPkg.installed? && depTask.action != "rebuild")
					dirtyDep = true;
				end
			}
		}
		
		startPkg.conflictsWithInstalled.each { |pkgName, pkg|
			if (!@tasksPerName.has_key?(pkgName) && @cache.has_key?(pkgName))
				task = Task.new(self, @cache[pkgName], "force-delete");
				@tasks.push(task);
				if (!@tasksPerName.has_key?(pkgName))
					@tasksPerName[pkgName] = Array.new()
				end
				@tasksPerName[pkgName].push(task);
				
				dependencies[pkgName] = task;
			end
		}
		
		if (startPkg.upgrade?)
			task = Task.new(self, startPkg, "upgrade");
		elsif (!startPkg.installed?)
			task = Task.new(self, startPkg, "install");
		elsif (dirtyDep || startPkg.rebuild?)
			task = Task.new(self, startPkg, "rebuild");
		else
			$curStatus[Thread.current] = tmpStat;
			log(prefix + "Nothing to do for " + startPkg.pkgName, "createTasksForPkg", 990);
			return dependencies.values();
		end
		
		startPkg.dependsInstalled.each { |depPkgName, depPkgHash|
			if (startPkg.buildDepends.has_key?(depPkgName) || startPkg.depends.has_key?(depPkgName))
				next;
			end
			
			task = Task.new(self, depPkgHash["pkg"], "delete");
			@tasks.push(task);
			if (!@tasksPerName.has_key?(depPkgName))
				@tasksPerName[depPkgName] = Array.new()
			end
			@tasksPerName[depPkgName].push(task);
			
			dependencies[pkgName] = task;
		}
		
		dependencies.each { |depTaskName, depTask|
			task.deps[depTask.pkg.pkgName] = depTask
			if (depTask.action == "install")
				log(startPkg.pkgName + " depends on missing package: " + depTask.pkg.pkgName, "createTasksForPkg", 0);
			end
		};
		
		@tasks.push(task);
		if (!@tasksPerName.has_key?(startPkg.pkgName))
			@tasksPerName[startPkg.pkgName] = Array.new()
		end
		@tasksPerName[startPkg.pkgName].push(task);
		
		log(prefix + "Returning task created for " + startPkg.pkgName, "createTasksForPkg", 990);
		$curStatus[Thread.current] = tmpStat;
		return [task];
	end
	
	def getOutput(cmd)
		log(cmd, "getOutput", 900);
		
		lines = nil;
		e = nil;
		status = Open4::popen4(cmd) { |pid, stdin, stdout, stderr|
			lines = stdout.readlines;
			e = stderr.readlines;
		}
		
		if (status.exitstatus != 0)
			unless e.nil?
				raise cmd + ": " + e.to_s();
			end
			
			raise "'" + cmd + "' had exit status: " + status.exitstatus.to_s
		end
		
		if lines.nil? || lines.empty?
			raise "No output for '" + cmd + "'";
		end
		
		return lines;
	end
	
	def loggedExec(cmd, fileName = nil)
		log(cmd, "loggedExec", 900);
		
		if (fileName == nil)
			fileNamePref = Dir::tmpdir() + "/";
			fileName = fileNamePref + "pkgsrc-" + rand(65536).to_s() + ".log";
			while (File.exist?(fileName))
				fileName = fileNamePref + "pkgsrc-" + rand(65536).to_s() + ".log";
			end
		end
		
		lines = nil;
		e = nil;
		status = Open4::popen4(cmd + " >> " + fileName + " 2>&1") { |pid, stdin, stdout, stderr|
			stdin.close();
			lines = stdout.readlines;
			e = stderr.readlines;
		}
		
		if (status.exitstatus != 0)
			unless e.nil?
				raise cmd + ":" + e.to_s();
			end
			
			raise "'" + cmd + "' had exit status: " + status.exitstatus.to_s
		end
		
		return fileName;
	end
end