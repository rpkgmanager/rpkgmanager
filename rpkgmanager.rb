#!/usr/bin/env ruby -Ku
# coding: utf-8

require 'pathname'
require 'getoptlong'

# settings
APP_VERSION = 0.1;
PREFIX = "/usr/pkg";
APP_ROOT = File.dirname(Pathname.new(__FILE__).realpath);
# wanted db
wantedFile = APP_ROOT + "/wanted.db";

#$stdout.sync = true;
Thread.abort_on_exception = true;

# prepare SIGINFO trap
$curStatus = Hash.new();
trap("INFO") {
	$curStatus.each { |thread, status|
		puts thread.to_s + ": " + status;
	}
}

def curTimeStr()
	return Time.new().strftime("%Y-%m-%d %H:%M:%S");
end

def log(msg, origin = nil, level = 0, output = STDOUT, prefix = curTimeStr())
	if ($debug_lvl >= level)
		if (origin != nil)
			prefix += " [" + origin + "]";
		end
		
		output.puts(prefix + ": " + msg);
	end
end

def warning(msg, origin = nil, level = 0)
	log(msg, origin, level, STDERR);
end

def error(msg, origin = nil, level = 0)
	log(msg, origin, level, STDERR);
end


opts = GetoptLong.new(
	['--help', '-h', GetoptLong::NO_ARGUMENT],
	['--dep-rebuild', GetoptLong::NO_ARGUMENT],
	['--strict-rebuild', GetoptLong::NO_ARGUMENT],
	['--show-options', GetoptLong::NO_ARGUMENT],
	['--gen-db', GetoptLong::NO_ARGUMENT],
	['--debug-level', GetoptLong::REQUIRED_ARGUMENT],
	['--clean-distfiles', GetoptLong::NO_ARGUMENT]
);

showOptions = false;
depRebuild = false;
strictRebuild = false;
genDb = false;
cleanDistfiles = false;
$debug_lvl = 50;

opts.each { |opt, arg|
	case opt
		when '--help'
			error("usage: rpkgmanager.rb [-h|--help] [--dep-rebuild] [--strict-rebuild] [--show-options|--gen-db] [--debug-level [FULL|DEBUG|WARNING]] [--clean-distfiles]");
			Kernel.exit(1);
		when '--show-options'
			if (genDb)
				error("Cannot specify both --show-options and --gen-db at the same time! Choose one!");
				Kernel.exit(1);
			end
			showOptions = true;
		when '--dep-rebuild'
			depRebuild = true;
		when '--strict-rebuild'
			strictRebuild = true;
		when '--gen-db'
			if (showOptions)
				error("Cannot specify both --show-options and --gen-db at the same time! Choose one!");
				Kernel.exit(1);
			end
			genDb = true;
		when '--debug-level'
			case arg.downcase()
				when 'full'
					$debug_lvl = 1000;
				when 'debug'
					$debug_lvl = 900;
				when 'warning'
					$debug_lvl = 200;
				else
					error("Invalid debug level!");
					Kernel.exit(1);
			end
		when '--clean-distfiles'
			cleanDistfiles = true;
	end
}


wantedList = Hash.new();
if (genDb)
	if (File.exists?(wantedFile))
		error("ERROR: wanted.db exists, but you wish to generate a new one.");
		Kernel.exit(1);
	end
else
	# read the wanted list
	log("Reading wanted.db...");
	if (!File.exists?(wantedFile))
		error("Could not find a list of wanted packages, please define them in wanted.db file.");
		Kernel.exit(1);
	elsif (!File.readable?(wantedFile))
		error("wanted.db file is not readable!");
		Kernel.exit(1);
	end
	
	IO.foreach(wantedFile) { |line|
		line.chomp!;
		if (line.empty? || line[0] == "#"[0])
			next;
		end;
		
		lineAr = line.split(",");
		if (lineAr.size != 2)
			raise "Invalid syntax in line: '" + line + "'";
		end
		
		lineAr[0].chomp!;
		lineAr[1].chomp!;
		prio = Integer(lineAr[1]);
		if (prio >= 1000)
			STDERR.puts line;
			raise "Priority cannot be >= 1000!";
		end
		
		wantedList[lineAr[0]] = prio;
	}
	log("Reading wanted.db... Done");
end


require APP_ROOT + '/pkgsrc'
pkgTools = Pkgsrc.new();
pkgTools.depRebuild = depRebuild;
pkgTools.strictRebuild = strictRebuild;

log("Gathering info...");
if (!showOptions)
	pkgTools.gatherInstalledPkgs();
end

if (genDb)
	dummyWantedList = Hash.new();
	pkgTools.findRootNodes().each { |pkgName, pkg|
		begin
			dummyWantedList[pkgName] = 1;
			pkgTools.gatherData(dummyWantedList);
		rescue => ex
			dummyWantedList.delete(pkgName);
			warning("WARNING: Could not properly fetch data for " + pkgName + " from pkgsrc!");
		end
	}
	
	pkgTools.cleanCache();
	pkgTools.gatherData(dummyWantedList);
	pkgTools.deleteRootNodes();
else
	pkgTools.gatherData(wantedList);
end
pkgTools.postGatherData();
log("Gathering info... Done");

if (genDb)
	log("Generating the wanted file...");
	rootNodes = pkgTools.findRootNodes().sort { |a, b| a[0] <=> b[0] };
	File.open(wantedFile, "w") { |file|
		rootNodes.each { |pkgName, pkg|
			file.puts pkgName + ",1";
		}
		
		log("Generating the wanted file... Done");
		log("NOTE: modify wanted.db according to your needs before using it! Can contain packages automatically installed by pkgsrc (build dependencies)!");
		Kernel.exit();
	}
	
	error(": Could not generate the wanted.db file!");
	Kernel.exit(1);
end

if (showOptions)
	log("Displaying PKG_OPTIONS info...");
	pkgTools.cache.each { |pkgName, pkg|
		if (pkg.pkgOptionsVar == nil || pkg.pkgOptionsAll == nil)
			next;
		end
		
		puts "Package " + pkgName + " with PKG_OPTIONS_VAR=" + pkg.pkgOptionsVar;
		print "\tAvailable options: "
		puts pkg.pkgOptionsAll;
		print "\tCurrently enabled: ";
		puts pkg.pkgOptions;
	}
	Kernel.exit();
end

log("Creating tasks...");
pkgTools.createTasks();
log("Creating tasks... Done");

while (true)
	print "Do you wish to continue? [y/n] ";
	input = STDIN.readline();
	input.chomp!;
	input.downcase!;
	
	if (input == "n")
		Kernel.exit();
	elsif (input == "y")
		break;
	end
end

# before quitting, we also want output the current state of affairs
at_exit { pkgTools.checkFailedTasks(); }

# first fetch all needed distfiles
log("Fetching needed distfiles...");
pkgTools.preExecTasks();
log("Fetching needed distfiles... Done");

# now actually execute tasks
log("Executing tasks...");
pkgTools.execTasks();
log("Executing tasks... Done");

if (cleanDistfiles)
	log("Removing obsolete distfiles...");
	pkgTools.cleanDistfiles();
	log("Removing obsolete distfiles... Done");
end

if (!pkgTools.failedTasks.empty?)
	Kernel.exit(1);
end