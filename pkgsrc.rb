require APP_ROOT + '/pkgtools'
require "open4"
require "date"


class Pkgsrc < AbstractPkgtools
	MAKE = PREFIX + "/bin/bmake"
	PKG_INFO = PREFIX + "/sbin/pkg_info"
	PERL = PREFIX + "/bin/perl"
	SRCTREE = "/usr/pkgsrc"
	
	ADDITIONALPKGS = {
		"pkgtools/pkg_install" => 1200,
		"pkgtools/bootstrap-mk-files" => 1151,
		"devel/bmake" => 1150,
		"pkgtools/digest" => 1100,
		"pkgtools/pkg_tarup" => 1100,
		"archivers/pax" => 1100,
		"pkgtools/p5-pkgsrc-Dewey" => 1011,
		"lang/ruby" => 1010,
		"devel/ruby-open4" => 1009,
	}
	
	SPECIALPKGS = ["pkgtools/bootstrap-mk-files", "devel/bmake", "archivers/pax"]
	
	
	def initialize()
		if (RUBY_VERSION=~ /1\.8/)
			ADDITIONALPKGS["misc/rubygems"] = 1005;
		end
		@binPkgTree = SRCTREE + "/packages"
		
		lines = getOutput("cd " + SRCTREE + "/" + ADDITIONALPKGS.keys[0] + " && " + MAKE + " show-vars VARNAMES='PACKAGES'");
		lines.each { |line|
			line.chomp!;
			if (line.empty? && at != 5)
				next;
			end
			
			@binPkgTree = line;
		}
		super();
	end
	
	def setWantList(wantList)
		super(wantList);
		ADDITIONALPKGS.each { |node, prio|
			@wantList[node] = prio;
		}
	end
	
	def getPkgData(pkg)
		tmpStat = $curStatus[Thread.current];
		$curStatus[Thread.current] = "Getting data about " + pkg;
		
		ret = getPackage(pkg);
		if (ret.pkgVer != nil)
			$curStatus[Thread.current] = tmpStat;
			return ret;
		end
		
		#lines = getOutput(MAKE + " -C " + SRCTREE + "/" + pkg + " print-summary-data");
		lines = getOutput("cd " + SRCTREE + "/" + pkg + " && " + MAKE + " print-summary-data");
		lines.each{ |line|
			lineAr = line.chomp().split(" ", 3);
			
			case lineAr[0]
				when "depends", "build_depends"
					if (lineAr[2].nil?)
						next;
					end
					
					lineAr[2].split().each { |val|
						valArr = val.split(":");
						pkgName = valArr[1][6, valArr[1].length];
						pkgVer = valArr[0];
						
						if (lineAr[0] == "depends")
							dependencies = ret.depends;
						else
							dependencies = ret.buildDepends;
						end
						dependencies[pkgName] = Hash.new();
						dependencies[pkgName]["constraint"] = pkgVer;
						dependencies[pkgName]["pkg"] = getPkgData(pkgName);
					}
				when "conflicts"
					if (lineAr[2].nil?)
						next;
					end
					lineAr[2].chomp!
					
					lineAr[2].split().each { |rawConflict|
						rawConflict.chomp!
						ret.conflicts.push(rawConflict);
					}
				when "index"
					ret.pkgVer = lineAr[2];
			end
		}
		
		# handle tool depends
		lines = getOutput("cd " + SRCTREE + "/" + pkg + " && " + MAKE + " show-vars VARNAMES='TOOL_DEPENDS'");
		lines.each{ |line|
			if (line.nil?)
				next;
			end
			
			line.chomp().split().each { |val|
				valArr = val.split(":");
				pkgName = valArr[1][6, valArr[1].length];
				pkgVer = valArr[0];
				
				dependencies = ret.buildDepends;
				dependencies[pkgName] = Hash.new();
				dependencies[pkgName]["constraint"] = pkgVer;
				dependencies[pkgName]["pkg"] = getPkgData(pkgName);
			}
		}
		
		# now we want to get the currently enabled PKG_OPTIONS
		lines = getOutput("cd " + SRCTREE + "/" + pkg + " && " + MAKE + " show-vars VARNAMES='PKG_OPTIONS_VAR PKG_SUPPORTED_OPTIONS PKG_OPTIONS DIST_SUBDIR DISTFILES PATCHFILES'");
		at = 0;
		
		distSubdir = nil;
		lines.each { |line|
			at += 1;
			line.chomp!;
			if (line.empty? && at != 5)
				next;
			end
			
			case (at)
				when 1
					ret.pkgOptionsVar = line;
				when 2
					ret.pkgOptionsAll = line;
				when 3
					ret.pkgOptions = line;
				when 4
					distSubdir = line;
				when 5
					line.split().each { |distfile|
						if (distSubdir == nil || distSubdir.empty?)
							ret.distfiles.push(distfile);
						else
							ret.distfiles.push(distSubdir + "/" + distfile);
						end
					}
				when 6
					line.split().each { |patchfile|
						if (distSubdir == nil || distSubdir.empty?)
							ret.distfiles.push(patchfile);
						else
							ret.distfiles.push(distSubdir + "/" + patchfile);
						end
					}
			end
		}
=begin
		if (!ret.conflicts.empty?)
			print ret.pkgVer + ": ";
			pp(ret.conflicts);
		end
=end
		$curStatus[Thread.current] = tmpStat;
		return ret;
	end
	
	def getInstalledPkgData(pkg)
		pkgName = nil;
		tmpStat = $curStatus[Thread.current];
		$curStatus[Thread.current] = "Getting data about installed package " + pkg;
		
		lines = getOutput(PKG_INFO + " -X '" + pkg + "'");
		lines.each { |line|
			lineAr = line.chomp().split("=", 2);
			
			case lineAr[0]
				when "PKGPATH"
					pkgName = lineAr[1];
					break;
			end
		}
		if (pkgName == nil)
			raise pkg + " not found!";
		end
		
		ret = getPackage(pkgName);
		if (ret.pkgVerInstalled != nil)
			$curStatus[Thread.current] = tmpStat;
			return ret;
		end
		
		lines.each { |line|
			lineAr = line.chomp().split("=", 2);
			if (lineAr[1] != nil)
				lineAr[1].chomp!
			end
			
			case lineAr[0]
				when "PKGNAME"
					ret.pkgVerInstalled = lineAr[1];
				when "DEPENDS"
					# we need to get a valid name for the dependency
					if (lineAr[1][0] == "{"[0])
						lineAr[1] = lineAr[1][1, lineAr[1].length - 1];
						deps = Array.new();
						
						curDep = "";
						startCurly = 0;
						lineAr[1].each_char { |x|
							if (x == "{")
								startCurly += 1;
								next;
							elsif (x == "}")
								startCurly -= 1;
								next;
							elsif (x != "," || startCurly > 0)
								curDep += x;
								next;
							end
							
							deps.push(curDep);
							curDep = "";
						}
						deps.push(curDep);
						
						found = false;
						deps.each { |dep|
							begin
								tmpAr = getInstalledPkgData(dep);
								ret.dependsInstalled[tmpAr.pkgName] = Hash.new();
								ret.dependsInstalled[tmpAr.pkgName]["constraint"] = dep;
								ret.dependsInstalled[tmpAr.pkgName]["pkg"] = tmpAr;
								
								found = true;
							rescue => ex
								if (!ex.message.chomp.match(/#{Regexp.escape("pkg_info: No matching pkg for " + dep + ".")}/) && !ex.message.chomp.match(/#{Regexp.escape("pkg_info: can't find package `" + dep + "'")}/))
									puts lines
									raise ex;
								end
								
								# the package does not exist in pkgsrc
							end
						}
						if (found == false)
							puts lines;
							puts deps;
							raise "None of the dependencies have been found!";
						end
					else
						begin
							tmpAr = getInstalledPkgData(lineAr[1]);
							ret.dependsInstalled[tmpAr.pkgName] = Hash.new();
							ret.dependsInstalled[tmpAr.pkgName]["constraint"] = lineAr[1];
							ret.dependsInstalled[tmpAr.pkgName]["pkg"] = tmpAr;
						rescue => ex
							if (!ex.message.chomp.match(/#{Regexp.escape("pkg_info: No matching pkg for " + lineAr[1] + ".")}/) && !ex.message.chomp.match(/#{Regexp.escape("pkg_info: can't find package `" + lineAr[1] + "'")}/))
								puts lines
								raise ex;
							end
							
							# the package does not exist in pkgsrc
						end
					end
				when "CONFLICTS"
					lineAr[1].chomp!
					ret.conflictsInstalled.push(lineAr[1]);
				when "BUILD_DATE"
					ret.buildDate = DateTime.parse(lineAr[1]);
				when "PKG_OPTIONS"
					if (!lineAr[1].empty?)
						ret.pkgOptionsInstalled = lineAr[1];
					end
				# these two will be very useful for later work on safe/non-destructive upgrades
				when "PROVIDES"
					ret.libProvides.push(lineAr[1]);
				when "REQUIRES"
					ret.libRequires.push(lineAr[1]);
			end
		}
=begin
		if (!ret.conflictsInstalled.empty?)
			puts ret.pkgVerInstalled +  + ": " + pp(ret.conflictsInstalled);
		end
=end
		
		$curStatus[Thread.current] = tmpStat;
		return ret;
	end
	
	def conflictCheck()
		conflicts = Hash.new();
		
		# first extract all the conflict patterns from the packages we want to have installed
		conflictsPerPkg = Hash.new();
		@cache.each { |pkgName, pkg|
			pkg.conflictsWithInstalled = Hash.new();
			
			if (pkg.conflicts.empty?)
				next;
			end
			
			conflictsPerPkg[pkgName] = Array.new();
			pkg.conflicts.each { |conflict| conflictsPerPkg[pkgName].push(conflict); }
		}
		
		# now check all pckages that we want to have installed, if they conflict with any of the conflict patterns
		@cache.each { |pkgName, pkg|
			conflictsPerPkg.each { |tmpPkgName, conflictArr|
				if (pkgName == tmpPkgName)
					next;
				end
				
				conflictArr.each { |conflict|
					if (pkg.pkgVer != nil && match?(conflict, pkg.pkgVer))
						if (!conflicts.has_key?(tmpPkgName))
							conflicts[tmpPkgName] = Hash.new();
						end
						if (!conflicts[tmpPkgName].has_key?(conflict))
							conflicts[tmpPkgName][conflict] = Array.new();
						end
						
						conflicts[tmpPkgName][conflict].push(pkgName)
					end
					
					if (pkg.pkgVerInstalled != nil && match?(conflict, pkg.pkgVerInstalled))
						@cache[tmpPkgName].conflictsWithInstalled[pkgName] = pkg;
					end
				}
			}
		}
		
		return conflicts;
	end
	
	def getInstalledPkgList()
		lines = getOutput(PKG_INFO + " -a");
		
		ret = Array.new();
		lines.each { |line|
			lineAr = line.chomp().split(" ", 2);
			ret.push(lineAr[0]);
		}
		ret
	end
	
	def getDirEntriesRec(root, relPath = nil)
		ret = Hash.new();
		
		path = root;
		if (relPath != nil)
			path += "/" + relPath;
		end
		
		Dir.foreach(path) { |entry|
			if (entry == "." || entry == "..")
				next;
			end
			
			# if we are at DISTDIR, hidden files/directories should be left alone, but hidden files/dirs in subdirs are a fair game
			if (relPath == nil && entry.start_with?("."))
				next;
			end
			
			if (File.directory?(path + "/" + entry))
				tmpPath = entry;
				if (relPath != nil)
					tmpPath = relPath + "/" + tmpPath;
				end
				
				getDirEntriesRec(root, tmpPath).each { |tmpEntry, tmp|
					ret[tmpEntry] = tmp;
				}
			else
				if (relPath != nil)
					ret[relPath + "/" + entry] = 1;
				else
					ret[entry] = 1;
				end
			end
		}
		
		ret
	end
	
	def cleanDistfiles()
		distdir = nil;
		
		lines = getOutput("cd " + SRCTREE + "/" + ADDITIONALPKGS.keys[0] + " && " + MAKE + " show-vars VARNAMES='DISTDIR'");
		lines.each { |line|
			distdir = line.chomp();
		}
		toCheckFiles = getDirEntriesRec(distdir);
		
		@cache.each { |pkgName, pkg|
			pkg.distfiles.each { |fileName| toCheckFiles.delete(fileName); }
		}
		
		toCheckFiles.keys.each { |distfile|
			log("Deleting: " + distdir + "/" + distfile, "cleanDistfiles", 700);
			File.delete(distdir + "/" + distfile);
			
			pathElements = distfile.split("/");
			if (pathElements.length > 1)
				tmpPath = pathElements[0...-1].join("/");
				
				if (Dir.entries(distdir + "/" + tmpPath).length == 2)
					log("Deleting: " + distdir + "/" + tmpPath, "cleanDistfiles", 700);
					Dir.rmdir(distdir + "/" + tmpPath);
				end
			end
		}
	end
	
	def match?(pattern, pkgVer)
		key = pattern + "\", \"" + pkgVer;
		
		if (@cacheMatchCheck == nil)
			@cacheMatchCheck = Hash.new();
		elsif (@cacheMatchCheck.has_key?(key))
			return @cacheMatchCheck[key];
		end
		
		if (@cacheMatchCheck.size() > 1000)
			@cacheMatchCheck.clear();
		end
		
		# first simple match ... we check if the alphanumeric parts of the string match
		partLen = pattern.index(/[^a-zA-Z0-9\-\_\.]/);
		if (partLen != nil  && pkgVer[0, partLen] != pattern[0, partLen])
			@cacheMatchCheck[key] = false;
			return false;
		end
		
		if (pattern == pkgVer)
			@cacheMatchCheck[key] = true;
			return true;
		end
		
		if (pattern.index(/\{/) != nil)
			@cacheMatchCheck[key] = alternate_match(pattern, pkgVer);
			#puts "alternate_match('" + pattern + "', '" + pkgVer + "') returned " + @cacheMatchCheck[key].to_s();
			return @cacheMatchCheck[key];
		end
		
		if (pattern.index(/[<>]/) != nil)
			lines = nil;
			e = nil;
			status = Open4::popen4(PERL) { |pid, stdin, stdout, stderr|
				stdin.write("use pkgsrc::Dewey;");
				stdin.write("print dewey_match(\"" + pattern + "\", \"" + pkgVer + "\");");
				stdin.close;
				
				lines = stdout.readlines;
				e = stderr.readlines;
			}
			
			if (status.exitstatus != 0)
				unless e.nil?
					raise e.to_s();
				end
				
				raise "'print dewey_match(\"" + pattern + "\", \"" + pkgVer + "\");' had exit status: " + status.exitstatus.to_s
			end
			
			if lines.nil? || lines.empty?
				raise "'print dewey_match(\"" + pattern + "\", \"" + pkgVer + "\");'";
			end
			
			if (lines.length == 1 && lines[0] == "1")
				@cacheMatchCheck[key] = true;
			else
				@cacheMatchCheck[key] = false;
			end
			#puts "dewey_match('" + pattern + "', '" + pkgVer + "') == " + @cacheMatchCheck[key].to_s();
			return @cacheMatchCheck[key];
		end
		
		if (pattern.index(/[\*\?\[\]]/) != nil)
			if ((pkgVer[0] == "."[0] && pattern[0] != "."[0]) || (pkgVer[0] != "."[0] && pattern[0] == "."[0]))
				#puts "File.fnmatch('" + pattern + "', '" + pkgVer + "') cannot be 0, leading periods don't match!";
			elsif (File.fnmatch(pattern, pkgVer))
				@cacheMatchCheck[key] = true;
				#puts "File.fnmatch('" + pattern + "', '" + pkgVer + "') returned " + @cacheMatchCheck[key].to_s().to_s();
				return @cacheMatchCheck[key];
			end
			#puts "File.fnmatch('" + pattern + "', '" + pkgVer + "') returned false";
		end
		
		tmpPattern = pattern + "-[0-9]*";
		if ((pkgVer[0] == "."[0] && tmpPattern[0] != "."[0]) || (pkgVer[0] != "."[0] && tmpPattern[0] == "."[0]))
			#puts "File.fnmatch('" + tmpPattern + "', '" + pkgVer + "') cannot be 0, leading periods don't match!";
		elsif (File.fnmatch(tmpPattern, pkgVer))
			@cacheMatchCheck[key] = true;
			#puts "File.fnmatch('" + tmpPattern + "', '" + pkgVer + "') returned " + @cacheMatchCheck[key].to_s();
			return @cacheMatchCheck[key];
		end
		
		#puts "File.fnmatch('" + tmpPattern + "', '" + pkgVer + "') returned false";
		@cacheMatchCheck[key] = false;
		return @cacheMatchCheck[key];
	end
	
	def alternate_match(pattern, pkgVer)
		# first check if there's a correct number of open and close curly braces
		curlyCnt = 0;
		pattern.each_char { |x|
			if (x == "{")
				curlyCnt += 1;
			elsif (x == "}")
				curlyCnt -= 1;
			end
		}
		if (curlyCnt != 0)
			raise pattern + " does not have the correct number of open and closed curly braces!";
		end
		
		# now do the real work
		prefix = pattern[0, pattern.index(/\{/)];
		suffix = pattern[pattern.rindex(/\}/) + 1, pattern.length];
		tmpPattern = "";
		curlyCnt = 0;
		found = false;
		
		pattern.each_char { |x|
			if (x == "{")
				curlyCnt += 1;
				if (curlyCnt == 1)
					tmpPattern = "";
				else
					tmpPattern += x;
				end
				next;
			elsif (x == "}")
				curlyCnt -= 1;
				if (curlyCnt > 0)
					tmpPattern += x;
					next;
				end
			elsif (x != ",")
				tmpPattern += x;
				next;
			end
			
			tmpPattern = prefix + tmpPattern + suffix;
			#puts pattern + " => (prefix = " + prefix + "; suffix = " + suffix + "; length = " + tmpPattern.length.to_s() + ") " + tmpPattern;
			if (match?(tmpPattern, pkgVer))
				#puts "match?(tmpPattern, pkgVer) returned true!";
				found = true;
				# we can break here (probably)
				break;
			end
			
			tmpPattern = "";
			if (curlyCnt == 0)
				break;
			end
		}
		
		return found;
	end
	
	
	def makeTarget(pkg, args, fileName)
		#return loggedExec(MAKE + " -C " + SRCTREE + "/" + pkg.pkgName + " " + args, fileName);
		return loggedExec("cd " + SRCTREE + "/" + pkg.pkgName + " && " + MAKE + " " + args, fileName);
	end
	
	def fetchDistfiles(pkg, fileName)
		tmpStat = $curStatus[Thread.current];
		
		$curStatus[Thread.current] = "Fetching distfiles for " + pkg.pkgName + "...";
		makeTarget(pkg, "checksum", fileName);
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def buildPkg(pkg, fileName, args = "")
		tmpStat = $curStatus[Thread.current];
		
		$curStatus[Thread.current] = "Cleaning " + pkg.pkgName + "...";
		makeTarget(pkg, "clean", fileName);
		
		$curStatus[Thread.current] = "Building " + pkg.pkgName + "...";
		makeTarget(pkg, args + " configure", fileName);
		
		$curStatus[Thread.current] = "Building " + pkg.pkgName + "...";
		makeTarget(pkg, args + " build", fileName);
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def replacePkg(pkg, fileName)
		tmpStat = $curStatus[Thread.current];
		
		if (SPECIALPKGS.include?(pkg.pkgName))
			buildPkg(pkg, fileName, "USE_DESTDIR=yes");
			makeTarget(pkg, "USE_DESTDIR=yes package", fileName);
		else
			buildPkg(pkg, fileName);
		end
		
		$curStatus[Thread.current] = "Making backup package of " + pkg.pkgName + "...";
		loggedExec(PREFIX + "/bin/pkg_tarup " + pkg.pkgVerInstalled, fileName);
		
		$curStatus[Thread.current] = "Replacing " + pkg.pkgName + " with newer version...";
		if (SPECIALPKGS.include?(pkg.pkgName))
			loggedExec(PREFIX + "/sbin/pkg_delete -ff " + pkg.pkgVerInstalled, fileName);
			loggedExec(PREFIX + "/sbin/pkg_add " + @binPkgTree + "/All/" + pkg.pkgVer + ".tgz", fileName);
		else
			makeTarget(pkg, "replace", fileName);
		end
		
		makeTarget(pkg, "clean", fileName);
		
		# delete the backup created with pkg_tarup!
		File.delete("/tmp/" + pkg.pkgVerInstalled + ".tgz");
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def installPkg(pkg, fileName)
		tmpStat = $curStatus[Thread.current];
		
		buildPkg(pkg, fileName);
		
		$curStatus[Thread.current] = "Installing " + pkg.pkgName + "...";
		makeTarget(pkg, "install", fileName);
		
		makeTarget(pkg, "clean", fileName);
		
		$curStatus[Thread.current] = tmpStat;
	end
	
	def deletePkg(pkg, fileName, force = false)
		loggedExec(PREFIX + "/sbin/pkg_delete " + (force ? "-f " : "") + pkg.pkgVerInstalled, fileName);
	end
end
