class Package
	attr_reader :pkgName, :priority;
	# info about the package in the pkgsrc tree
	attr_accessor :pkgVer, :depends, :buildDepends, :requiredBy, :conflicts, :conflictsWithInstalled, :pkgOptions, :pkgOptionsAll, :pkgOptionsVar, :distfiles;
	# info about the installed package
	attr_accessor :pkgVerInstalled, :dependsInstalled, :requiredByInstalled, :conflictsInstalled, :pkgOptionsInstalled, :libProvides, :libRequires;
	# more info about the installed package
	attr_accessor :buildDate;
	
	
	def initialize(pkgTools, pkgName)
		@pkgName = pkgName;
		@pkgTools = pkgTools;
		
		@pkgVer = nil;
		
		@priority = 0;
		
		@pkgOptions = nil;
		@pkgOptionsAll = nil;
		@pkgOptionsVar = nil;
		
		@depends = Hash.new();
		@buildDepends = Hash.new();
		@requiredBy = Hash.new();
		@conflicts = Array.new();
		@conflictsWithInstalled = Hash.new();
		@distfiles = Array.new();
		
		@pkgVerInstalled = nil;
		@pkgOptionsInstalled = nil;
		
		@dependsInstalled = Hash.new();
		@requiredByInstalled = Hash.new();
		@conflictsInstalled = Array.new();
		
		@libProvides = Array.new();
		@libRequires = Array.new();
	end
	
	def installed?()
		@pkgVerInstalled != nil;
	end
	
	def orphan?()
		@pkgVer == nil;
	end
	
	def upgrade?()
		if (@upgradeCache != nil)
			return @upgradeCache;
		end
		
		if (orphan? || !installed?)
			@upgradeCache = false;
			return false;
		end
		
		if (!@pkgTools.match?(@pkgVerInstalled, @pkgVer))
			puts "For " + @pkgName + " version differs! (" + @pkgVer + " - ports - vs. " + @pkgVerInstalled + " - installed)";
			@upgradeCache = true;
			return true;
		end
		
		if (@pkgOptions != @pkgOptionsInstalled)
			print "For " + @pkgName + " options differ ('";
			print @pkgOptions;
			print "' - ports - vs. '";
			print @pkgOptionsInstalled;
			print "' - installed)!\n";
			@upgradeCache = true;
			return true;
		end
		
		if (@depends.keys.sort != @dependsInstalled.keys.sort)
			print "For " + @pkgName + " dependencies differ ('";
			pp(@depends.keys);
			print "' vs. '";
			pp(@dependsInstalled.keys);
			print "')!\n";
			@upgradeCache = true;
			return true;
		end
		
		@upgradeCache = false;
		return false;
	end
	
	def rebuild?()
		if (@rebuildCache != nil)
			return @rebuildCache;
		end
		
		tmpSoftlinks = Hash.new();
		@libRequires.each { |lib|
			if (!File.exists?(lib))
				print "For " + @pkgName + " one of the libs has been removed ('" + lib + "')!\n"
				@rebuildCache = true;
				next;
			end
			
			if (!lib.match(/^\/usr\/lib\/lib.+\.so(\.[0-9]+)+$/))
				next;
			end
			
			# extract the softlink name
			softlink = lib.match(/^\/usr\/lib\/lib.+\.so/)[0];
			log("softlink = " + softlink, @pkgName + ".rebuild?", 900);
			if (!File.symlink?(softlink))
				next;
			end
			
			if (!tmpSoftlinks.has_key?(softlink))
				tmpSoftlinks[softlink] = Array.new();
			end
			tmpSoftlinks[softlink].push(lib);
		}
		
		tmpSoftlinks.each { |softlink, libs|
			if (libs.length > 1)
				puts "WARNING: " + @pkgName + " depends on more than one version of " + softlink + "!";
				next;
			end
			
			realfile = File.readlink(softlink);
			if (realfile[0,0] != "/")
				realfile = "/usr/lib/" + realfile;
			end
			log("realfile = " + realfile, @pkgName + ".rebuild?", 900);
			
			if (realfile != libs[0])
				print "For " + @pkgName + " one of the system libs has had a version bump ('";
				print libs[0]
				print "' => '"
				print realfile
				print "')!\n"
				@rebuildCache = true;
			end
		}
		
		if (@buildDate == nil)
			puts "For " + @pkgName + " (" + @pkgVerInstalled + ") BUILD_DATE is missing!";
			@rebuildCache = true;
		end
		
		if (@rebuildCache != nil)
			return @rebuildCache;
		end
		
		@rebuildCache = false;
		return false;
	end
	
	def priority=(newPrio)
		@priority = newPrio;
		@depends.each { |depPkgName, depPkgHash|
			if (depPkgHash["pkg"].priority < newPrio)
				depPkgHash["pkg"].priority = newPrio;
			end
		}
		
		@buildDepends.each { |depPkgName, depPkgHash|
			if (depPkgHash["pkg"].priority < newPrio)
				depPkgHash["pkg"].priority = newPrio;
			end
		}
	end
end