class Task
	attr_reader :pkg, :action, :status, :error, :errorFile;
	attr_accessor :deps;
	
	def initialize(pkgTools, pkg, action)
		@pkgTools = pkgTools;
		@pkg = pkg;
		@action = action;
		
		@deps = Hash.new();
		@status = nil;
		@error = nil;
		@errorFile = nil;
		
		case (action)
			when "delete", "force-delete"
				@pkg.depends.each { |depPkgName, depPkgHash| depPkgHash["pkg"].requiredBy.delete(@pkg.pkgName); }
				@pkg.buildDepends.each { |depPkgName, depPkgHash| depPkgHash["pkg"].requiredBy.delete(@pkg.pkgName); }
				@pkg.dependsInstalled.each { |depPkgName, depPkgHash| depPkgHash["pkg"].requiredByInstalled.delete(@pkg.pkgName); }
				#@pkg.pkgVerInstalled = nil;
				@pkg.dependsInstalled = Hash.new();
				@pkg.requiredByInstalled = Hash.new();
				@pkg.conflictsInstalled = Array.new();
				@pkg.pkgOptionsInstalled = nil;
				@pkg.libProvides = Array.new();
				@pkg.libRequires = Array.new();
				
				@pkgTools.cache.delete(@pkg.pkgName);
			when "upgrade", "install", "rebuild"
				@pkg.dependsInstalled.each { |depPkgName, depPkgHash| depPkgHash["pkg"].requiredByInstalled.delete(@pkg.pkgName); }
				@pkg.dependsInstalled.clear();
				
				@pkg.depends.each { |depPkgName, depPkgHash|
					depPkgHash["pkg"].requiredByInstalled[@pkg.pkgName] = @pkg;
					@pkg.dependsInstalled[depPkgName] = depPkgHash;
				}
				@pkg.buildDepends.each { |depPkgName, depPkgHash|
					depPkgHash["pkg"].requiredByInstalled[@pkg.pkgName] = @pkg;
					@pkg.dependsInstalled[depPkgName] = depPkgHash;
				}
		end
	end
	
	def preExec(outputPref = nil)
		case (action)
			when "upgrade", "install", "rebuild"
				if (outputPref != nil)
					print outputPref + " ";
				end
				print "Fetching for task " + @pkg.pkgName + "... ";
				
				fileNamePref = Dir::tmpdir() + "/pkgsrc-" + @pkg.pkgName.split("/")[1] + "-";
				fileName = fileNamePref + rand(65536).to_s() + ".log";
				while (File.exist?(fileName))
					fileName = fileNamePref + rand(65536).to_s() + ".log";
				end
				
				begin
					@pkgTools.fetchDistfiles(@pkg, fileName)
					puts "Done";
				rescue => ex
					puts "Error";
					@pkgTools.failedTasks.push(self);
					@error = ex;
					@errorFile = fileName;
					
					@status = false;
					return false;
				end
				
				File.delete(fileName);
		end
		
		return true;
	end
	
	def exec(outputPref = nil)
		if (@status == false)
			return false;
		end
		
		if (outputPref != nil)
			print outputPref + " ";
		end
		case (action)
			when "delete"
				print "Deleting " + @pkg.pkgName + "... ";
			when "force-delete"
				print "Forced deleting of " + @pkg.pkgName + "... ";
			when "upgrade"
				print "Upgrading:\t" + @pkg.pkgVerInstalled + " => " + @pkg.pkgVer + "... ";
			when "install"
				print "Installing:\t" + @pkg.pkgVer + "... ";
			when "rebuild"
				print "Rebuilding:\t" + @pkg.pkgVerInstalled + "... ";
		end
		
		if (!@deps.empty?)
			failedDeps = Array.new();
			@deps.each { |taskId, task|
				if (task.status != true)
					failedDeps.push(task.pkg.pkgName);
				end
			}
			
			if (!failedDeps.empty?)
				puts "Error in dependency";
				@pkgTools.failedTasks.push(self);
				
				failedDepsStr = "";
				PP.singleline_pp(failedDeps, failedDepsStr);
				
				@error = StandardError.new("The following dependencies failed: " + failedDepsStr);
				@errorFile = nil;
				
				@status = false;
				return false;
			end
		end
		
		fileNamePref = Dir::tmpdir() + "/pkgsrc-" + @pkg.pkgName.split("/")[1] + "-";
		fileName = fileNamePref + rand(65536).to_s() + ".log";
		while (File.exist?(fileName))
			fileName = fileNamePref + rand(65536).to_s() + ".log";
		end
		
		case (action)
			when "delete"
				begin
					@pkgTools.deletePkg(@pkg, fileName);
				rescue => ex
					puts "Error";
					@pkgTools.failedTasks.push(self);
					@error = ex;
					@errorFile = fileName;
					
					@status = false;
					return false;
				end
			when "force-delete"
				begin
					@pkgTools.deletePkg(@pkg, fileName, true);
				rescue => ex
					puts "Error";
					@pkgTools.failedTasks.push(self);
					@error = ex;
					@errorFile = fileName;
					
					@status = false;
					return false;
				end
			when "upgrade", "install", "rebuild"
				begin
					if (action == "upgrade")
						@pkgTools.replacePkg(@pkg, fileName);
					elsif (action == "rebuild")
						@pkgTools.replacePkg(@pkg, fileName);
					else
						@pkgTools.installPkg(@pkg, fileName);
					end
					
					@pkg.pkgVerInstalled = @pkg.pkgVer;
				rescue => ex
					puts "Error";
					@pkgTools.failedTasks.push(self);
					@error = ex;
					@errorFile = fileName;
					
					@status = false;
					return false;
				end
		end
		
		puts "Done";
		File.delete(fileName);
		@status = true;
		return true;
	end
	
	def status()
		return @status;
	end
end